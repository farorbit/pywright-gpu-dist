# PyWright
Python game engine for running visual novel games similar to the Phoenix Wright series

This program, a 'case maker', was created to enable fans to produce their own fan cases based on the Phoenix Wright series of games. It consists of an engine to run the games. Games themselves are scripted in "wrightscript", and several (old versions) of them can be downloaded from within the program itself.

# Features
* Fully functional scripting language, "WrightScript", based on a mishmash of BASIC and Python
* Animated sprites and text with sophisticated dialog engine
* Music, sound effects, other special effects
* Customizable
* Multiple saved games
* Works identically on Windows, Mac, Linux and Android
* Stay up to date by downloading engine patches and supported games from the engine itself
* Multiple display options

# History
This project was started in 2007 as a personal project by Saluk. After showing it to other Phoenix Wright fans on the court-records forums, it has grown and evolved through their feedback. It is the dedicated members there who have actually made the games that are available.

# Minimum requirements for building/running from source
You will need the following:
- Python 3.9
- Pygame 2.0.0
- NumPy 1.9.5

(Also check the requirements.txt file)

To pack the file into an exe, you can use cxFreeze with setup.py build for Linux or Windows.

This version has Android functionality built in. Unfortunately, I don't know how to compile it. (I might have a lead though on what he used)

# NOTES:
The reason why a older version may need to be used is (as of this writing the most recent version is 1.9.5) because of the color tinting problem.
If you suffer from this, use an older version. Otherwise, a new NumPy could be used.

Also if you suffer problems from missing SimpleJSON, then you can do install so using PIP.

# License
Under the New BSD License. This engine uses some assets by Capcom. I might remove/change them from this repo to avoid some problems.

# Credits
- saluk - original author
- CRxTRDude - maintainance
- Scoopa - helped find many bugs

# New Library
This version has added gpu acceleration with pygame 2.
